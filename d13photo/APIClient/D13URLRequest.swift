//
//  D13URLRequest.swift
//  d13photo
//
//  Created by Dhan Moti on 29/7/18.
//  Copyright © 2018 DeviDhan. All rights reserved.
//

import UIKit

class D13URLRequest: NSMutableURLRequest {

    convenience init(url: Foundation.URL) {
        self.init(url: url)
        
        self.url = url
        self.httpMethod = "POST"
        self.setValue(CommonUtil.getAuthHeader(), forHTTPHeaderField: "Authorization")
        
    }
    
    override init(url URL: URL, cachePolicy: NSURLRequest.CachePolicy, timeoutInterval: TimeInterval) {
        super.init(url: URL, cachePolicy: cachePolicy, timeoutInterval: timeoutInterval)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
