//
//  APIClient.swift
//  d13photo
//
//  Created by Dhan Moti on 22/7/18.
//  Copyright © 2018 DeviDhan. All rights reserved.
//

import UIKit

// authenticate using key and password
// Ref: https://help.shopify.com/en/api/reference/products/product#show

class APIClient: NSObject {
    
    class var share: APIClient {
        struct Singleton {
            static let instance = APIClient()
        }
        return Singleton.instance
    }
    
    typealias CompletionBlock = (_ result: Data?, _ error: Error?) -> Void
    var completion: CompletionBlock = { result, error in print(error ?? "") }
    
    var session = URLSession.shared
    
    override init() {
        super.init()
        
        let config = URLSessionConfiguration.default
        session = URLSession(configuration: config)
    }
    
    func getCollections(CompletionHandler: @escaping CompletionBlock) -> Void {
        
        guard let url = URL(string: "\(Constant.basicURL)custom_collections.json")
            else {
                return
        }
        
        var request = URLRequest(url: url)
        request.setValue(CommonUtil.getAuthHeader(), forHTTPHeaderField: "Authorization")
        
        // Add Request to session data task
        let task = session.dataTask(with: request) { (data, urlResponse, error) in
            
            guard let data = data else { return }
            do {
                // JSON Data to Swift Struct in one line. Wowooooo...
                let list = try JSONDecoder().decode(Collections.self, from: data)
                debugPrint("collections count: \(list.collections.count)")
                
            } catch let err {
                debugPrint(err)
            }
        }
        
        task.resume()
        
    }
    
    func getProductsFor(collectionId:String, CompletionHandler: @escaping CompletionBlock) -> Void {
        
        guard let url = URL(string: "\(Constant.basicURL)products.json?collection_id=\(collectionId)")
            else {
                return
        }
        
        var request = URLRequest(url: url)
        request.setValue(CommonUtil.getAuthHeader(), forHTTPHeaderField: "Authorization")
        
        // Add Request to session data task
        let task = session.dataTask(with: request) { (data, urlResponse, error) in
            guard let data = data else { return }
            
            do {
                // JSON Data to Swift Struct in one line. Wowooooo...
                let list = try JSONDecoder().decode(Products.self, from: data)
                debugPrint("prodcuts count: \(list.products.count)")
                
            } catch let err {
                debugPrint(err)
            }
        }
        task.resume()
        
    }
    
}
