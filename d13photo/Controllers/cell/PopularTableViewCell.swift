//
//  PopularTableViewCell.swift
//  d13photo
//
//  Created by Dhan Moti on 23/7/18.
//  Copyright © 2018 DeviDhan. All rights reserved.
//

import UIKit

class PopularTableViewCell: UITableViewCell {
    static let reuseIdentifier = "popular-cell"
    @IBOutlet weak var collectionView: UICollectionView!
    
 
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        collectionView.delegate = self
        collectionView.dataSource = self
        
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension PopularTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCollectionViewCell.resueIdentifier, for: indexPath)
        return cell
    }
}
