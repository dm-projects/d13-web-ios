//
//  ProductCollectionViewCell.swift
//  d13photo
//
//  Created by Dhan Moti on 23/7/18.
//  Copyright © 2018 DeviDhan. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    static let resueIdentifier = "product-collection-cell"
    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var loveCountLabel : UILabel!
    @IBOutlet weak var viewCountLabel : UILabel!
    @IBOutlet weak var purchaseCountLabel : UILabel!
    
    
}
