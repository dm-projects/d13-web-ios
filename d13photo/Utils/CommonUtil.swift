//
//  CommonUtil.swift
//  d13photo
//
//  Created by Dhan Moti on 22/7/18.
//  Copyright © 2018 DeviDhan. All rights reserved.
//

import UIKit

class CommonUtil: NSObject {

    static func getAuthHeader() -> String {
        guard let loginData = "\(Constant.shopifyAPIKey):\(Constant.shopifyPassword)".data(using: String.Encoding.utf8) else {
            return ""
        }
        return "Basic \(loginData.base64EncodedString())"
    }
}
