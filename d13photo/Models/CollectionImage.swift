//
//  CollectionImage.swift
//  d13photo
//
//  Created by Dhan Moti on 4/8/18.
//  Copyright © 2018 DeviDhan. All rights reserved.
//

struct CollectionImage: Codable {
    var width: Int
    var height: Int
    var src: String
}
