//
//  Image.swift
//  d13photo
//
//  Created by Dhan Moti on 4/8/18.
//  Copyright © 2018 DeviDhan. All rights reserved.
//

struct Image: Codable {
    var id: Int
    var productId: Int?
    var position: Int?
    var src: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case productId = "product_id"
        case position
        case src
    }
}
