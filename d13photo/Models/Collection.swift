//
//  Collection.swift
//  d13photo
//
//  Created by Dhan Moti on 29/7/18.
//  Copyright © 2018 DeviDhan. All rights reserved.
//

struct Collection: Codable {
    var id: Int?
    var title: String?
    var bodyHTML: String?
    var image: CollectionImage?
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case bodyHTML = "body_html"
        case image
    }
}

struct Collections: Codable {
    var collections: [Collection]
    
    enum CodingKeys: String, CodingKey {
        case collections = "custom_collections"
    }
}
