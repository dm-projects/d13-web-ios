//
//  Variant.swift
//  d13photo
//
//  Created by Dhan Moti on 4/8/18.
//  Copyright © 2018 DeviDhan. All rights reserved.
//

struct Variant: Codable {
    var id: Int
    var productId: Int?
    var title: String?
    var price: String?
    var position: Int?
    var imageId: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case productId = "product_id"
        case title
        case price
        case position
        case imageId = "image_id"
    }
}
