//
//  Product.swift
//  d13photo
//
//  Created by Dhan Moti on 29/7/18.
//  Copyright © 2018 DeviDhan. All rights reserved.
//

struct Product: Codable {
    var id: Int
    var title: String?
    var variants: [Variant]?
    var handle: String?
    var images: [Image]?
    var bodyHTML: String?
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case variants
        case handle
        case images
        
        case bodyHTML = "body_html"
    }
}

struct Products: Codable {
    var products: [Product]
}
