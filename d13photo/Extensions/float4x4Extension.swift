//
//  float4x4Extension.swift
//  d13photo
//
//  Created by Dhan Moti on 4/8/18.
//  Copyright © 2018 DeviDhan. All rights reserved.
//

import Foundation
import ARKit

extension float4x4 {
    var translation: float3 {
        let translation = self.columns.3
        return float3(translation.x, translation.y, translation.z)
    }
}
